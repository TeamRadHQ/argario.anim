(function(){
	function Background() {
		this.x      = 0;
		this.y      = 0;
		this.dy     = 0;		
	}
	Background.prototype.init = function() {
		this.img    = new Image();
		this.img.src = 'img/background.gif';
		this.canvas = document.getElementById('bgCanvas');
		this.ctx = this.canvas.getContext('2d');
		this.ctx.scale(1,1);
		this.canvas.cols = Math.ceil(this.canvas.width / this.img.width);
		this.canvas.rows = Math.ceil(this.canvas.height / this.img.height);	
	};
	Background.prototype.draw = function() {
		this.ctx.clearRect(0,0, this.canvas.width, this.canvas.height);
		for (cc=0; cc < this.canvas.cols; cc++) {
			for ( rr=0; rr < this.canvas.rows; rr++) {
				// Draw image at col.x and row.y
				this.ctx.drawImage(
					this.img,
					cc * this.img.width,
					this.y + rr * this.img.height,
					this.img.width,
					this.img.height
				);
				// Draw an image this.img.height behind this
				this.ctx.drawImage(
					this.img,
					cc * this.img.width,
					this.y - (1 * this.img.height),
					this.img.width,
					this.img.height
				);
				// Draw an image 2 * this.img.height behind this
				this.ctx.drawImage(
					this.img,
					cc * this.img.width,
					this.y - (2 * this.img.height),
					this.img.width,
					this.img.height
				);
			}
		} // End for col
		// Reset Scroll if exceeding canvas height
		if ( this.y < this.canvas.rows * this.img.height - 1) {
			this.y+=this.dy;
		} else {
			this.y=0;
		}
		window.requestAnimationFrame(bg.draw());
	}; // End draw()

	var bg = new Background();
	bg.init();
	bg.draw();
	bg.img.onload = function() {
		window.requestAnimationFrame(function(){bg.draw();});
	};// End init()
}());