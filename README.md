# Agario Animation

Recently I worked on a film project which required some animations. I thought it would be fun to do all of the animations using JavaScript canvas drawings!

This repository contains:

* An object prototype called `Drawable` which is extended to create the film "actors".

* A rendering engine which uses PHP to capture frames and store them in PNG format. 

The animation assets are not complicated at all. There are methods for moving objects in cardinal directions [N, NE, E, SE, S, SW, W, NW] and for drawing grids and circles (as this was all that was required for the project). 