/**
 * A repository for images
 */
var imgRepo = new function() {
	this.bg     = new Image();
	this.bg.src = "img/background.gif";
};

/**
 * Abstract class for drawable objects.
 */
function Drawable() {
	this.init = function(x,y) {
		this.x = x;
		this.y = y;
	};
	this.speed = 0;
	this.canvasWidth = 0;
	this.canvasHeight = 0;
	this.draw = function() {};
}
function Background() {
	this.speed = 2.5;
	this.canvas = $('canvas');
	$('body').append(this.canvas);

	this.draw = function() {
		// Tilt background
		this.y += this.speed;
		// this.x += this.speed;
		// console.log(imgRepo.bg);
		this.context.clearRect(0,0,imgRepo.bg.width,imgRepo.bg.height);
		this.context.drawImage(imgRepo.bg, this.x, this.y);
		this.context.drawImage(imgRepo.bg, this.x, this.y - (2 * imgRepo.bg.height));
		this.context.drawImage(imgRepo.bg, this.x, this.y - imgRepo.bg.height);
		this.context.drawImage(imgRepo.bg, this.x, this.y + imgRepo.bg.height);

		this.context.drawImage(imgRepo.bg, imgRepo.bg.width + this.x, this.y);
		this.context.drawImage(imgRepo.bg, imgRepo.bg.width + this.x, this.y - (2 * imgRepo.bg.height));
		this.context.drawImage(imgRepo.bg, imgRepo.bg.width + this.x, this.y - imgRepo.bg.height);
		this.context.drawImage(imgRepo.bg, imgRepo.bg.width + this.x, this.y + imgRepo.bg.height);

		this.context.drawImage(imgRepo.bg, (2 * imgRepo.bg.width) + this.x, this.y);
		this.context.drawImage(imgRepo.bg, (2 * imgRepo.bg.width) + this.x, this.y - (2 * imgRepo.bg.height));
		this.context.drawImage(imgRepo.bg, (2 * imgRepo.bg.width) + this.x, this.y - imgRepo.bg.height);
		this.context.drawImage(imgRepo.bg, (2 * imgRepo.bg.width) + this.x, this.y + imgRepo.bg.height);

		// If the image is offscreen reset
		if (this.y >= 2 * imgRepo.bg.height ) {
			this.y = 0;
		}
	};
}
Background.prototype = new Drawable();

function Game() {
	this.init = function() {
		this.bgCanvas = document.getElementById('background');
		// Compatibility check
		if (this.bgCanvas.getContext) {
			
			this.bgContext = this.bgCanvas.getContext('2d');
			Background.prototype.context = this.bgContext;
			Background.prototype.canvasWidth = this.bgCanvas.width;
			Background.prototype.canvasHeight = this.bgCanvas.height;

			this.bg = new Background();
			this.bg.init(0,0); // Set draw point to 0
			return true;
		} else { 
			return false;
		}
	};

	// Start the animation loop
	this.start = function() {
		animate();
	};
}

function animate() {
	requestAnimFrame( animate );
	game.bg.draw();
}

/**
 * requestAnim shim layer by Paul Irish
 */
window.requestAnimFrame = (function(){
	return 	window.requestAnimationFrame 	||
			window.webkitRequestAnimationFrame 	||
			window.mozRequestAnimationFrame		||
			window.oRequestAnimationFrame		||
			window.msRequestAnimationFrame		||
			function(/* function */ callback, /* DOMElement */ element){
				window.setTimeout(callback, 1000 / 60);
			};
})();

var game = new Game();

function init() {
	if (game.init()) {
		game.start();
	}
}