function HelloKitty() {
	this.scale  = 0.5;
	this.speed  = 5;
	this.repeat = true;
	this.id     = 'kitty';
	this.name = 'HelloKitty1995!';
	this.fillColor = 'pink';
	this.strokeColor = 'hotPink';

	this.glideLength = 0.5;


	this.subInit = function(){
		// Set the circle radius and calculate offsets
		this.radius    = this.canvasHeight/32;
		this.glide = this.radius / 4;
		console.log(this.glide);
		this.x         = [this.canvasWidth/2];
		this.y         = [this.canvasHeight/2];
		this.maxHeight = this.canvasHeight / 5;
	};

	this.draw = function() {
		if (this.inBounds) {
			if (this.radius < this.maxHeight) {
				this.circle();
				this.radius = this.radius + 0.15;
			} else {
				this.clear(); 
				return;
			}

			//this.debug;
			if (this.posX >= this.startX + this.glide && this.glideLength > 0) {
				this.glideLength = -this.glideLength;
			} else if (this.posX <= this.startX - this.glide && this.glideLength < 0) {
				this.glideLength = -this.glideLength;
			}
			this.glide = this.radius / 4;
			this.x = this.x + this.glideLength;


			// if(this.posX < this.startX + this.glide && this.glideLength > 0) {
			// 	this.x = this.x + this.glideLength;
			// } else if (this.posX == this.startX + this.glide) {
			// 	this.glideLength = - this.glideLength;
			// }
		}
		window.requestAnimationFrame(this.draw.bind(this));
	};

	this.styles = function(){
		this.lineWidth     = this.scaleVal(15);
		this.fillStyle     = this.fillColor;
		this.strokeStyle   = this.strokeColor;
		this.shadowBlur    = this.scaleVal(150);
		this.shadowColor   = "rgba(55,55,55,0.25)";
	};

	Object.defineProperty(this, 'debug', {
		get: function() { 
			this.passes+=1;
			this.t2 = (performance.now() - this.t1) / 1000;
			this.tTotal = this.tTotal + this.t2;
			this.tAvg = this.tTotal / this.passes;
			
			if (this.t2 < this.tMin) {
				this.tMin = this.t2;
				this.$debugTable.find('#debugMin').html(this.tMin.toFixed(3) + ' sec');
			}
			if (this.t2 > this.tMax) {
				this.tMax = this.t2;
				this.$debugTable.find('#debugMax').html(this.tMax.toFixed(3) + ' sec');
			}

			
			this.$debugTable.find('#debugRadius').html(this.radius.toFixed(2));
			this.$debugTable.find('#debugX').html(this.x.toFixed(2));
			this.$debugTable.find('#debugY').html(this.y.toFixed(2));
			this.$debugTable.find('#debugDX').html(this.dx.toFixed(2));
			this.$debugTable.find('#debugDY').html(this.dy.toFixed(2));
			this.$debugTable.find('#debugTime').html(this.tTotal.toFixed(3) + ' sec');
			
			// console.log('Pass: ' + this.passes );
		
			this.direction = (this.direction < 7) ? this.direction + 1 : 0;
			this.t1 = performance.now();
			return this.t2; 
		},
		set: function(t){ 
			if (t>0) {
				this.t1 = performance.now();
				console.log('Debugging...');
			}
			if (! this.passes ) {
				this.direction = 0;
				this.dirTxt = 'left';
				this.passes = 0;
				this.tTotal = 0;
				this.tAvg = 0;
				this.tMax = 0;
				this.tMin = 100000000000;
				this.$debugTable = $('<table></table>').attr('id', 'debugTable');
				
				$tr = $('<tr></tr>');
				$tdTitle = $('<td></td>').html('Title');
				$tdContent = $('<td></td>').attr('id', 'id').html('Title');

				$passes = $tr.clone();
				$passes.append($tdTitle.html('Pass:').clone());
				$passes.append($('<td></td>').attr('id', 'debugPasses').html(this.passes));
				this.$debugTable.append($passes);

				$time = $tr.clone();
				$time.append($tdTitle.html('Time:').clone());
				$time.append($('<td></td>').attr('id', 'debugTime').html('0.000 sec'));
				this.$debugTable.append($time);
				
				$radius = $tr.clone();
				$radius.append($tdTitle.html('r:').clone());
				$radius.append($('<td></td>').attr('id', 'debugRadius').html(this.radius));
				this.$debugTable.append($radius);				
				
				$x = $tr.clone();
				$x.append($tdTitle.html('x:').clone());
				$x.append($('<td></td>').attr('id', 'debugX').html(this.x.toFixed(2)));
				this.$debugTable.append($x);

				$y = $tr.clone();
				$y.append($tdTitle.html('y:').clone());
				$y.append($('<td></td>').attr('id', 'debugY').html(this.y.toFixed(2)));
				this.$debugTable.append($y);

				$dx = $tr.clone();
				$dx.append($tdTitle.html('dx:').clone());
				$dx.append($('<td></td>').attr('id', 'debugDX').html(this.dx.toFixed(2)));
				this.$debugTable.append($dx);

				$dy = $tr.clone();
				$dy.append($tdTitle.html('dy:').clone());
				$dy.append($('<td></td>').attr('id', 'debugDY').html(this.dy.toFixed(2)));
				this.$debugTable.append($dy);
				
				this.$debugTable.appendTo('body');
			}
		}
	});
}

function animate() {
	window.requestAnimationFrame(flab.draw);
}

HelloKitty.prototype = new Drawable();
