function FlabsWorth() {
	this.scale  = 0.5;
	this.speed  = 5;
	this.repeat = true;
	this.id     = 'flabsWorth';
	this.name   = 'FlabsWorth$$';

	this.fillColor     = "blue";
	this.strokeColor   = "navy";

	this.subInit = function(){
		// Set the circle radius and calculate offsets
		this.radius = 1.25 * this.canvasHeight;
	};

	this.draw = function() {
		if (this.inBounds) {
			this.circle();
			this.x += this.speed * this.dx;
			this.y += this.speed * this.dy;
		} else if (this.repeat) {
			this.x = this.startX;
			this.y = this.startY;
			// this.debug;
		} else {
			this.clear();
			return;
		}
		window.requestAnimationFrame(this.draw.bind(this));
	};

	this.styles = function(){
		this.lineWidth     = this.scaleVal(15);
		this.fillStyle     = this.fillColor;
		this.strokeStyle   = this.strokeStyle;
		this.shadowBlur    = this.scaleVal(150);
		this.shadowColor   = "rgba(55,55,55,0.25)";
		
		// Calculate shadow direction		
		if (this.dx && this.dy) {
			this.shadowOffsetX = (this.radius * this.dxInt) / 1.5;
			this.shadowOffsetY = (this.radius * this.dyInt) / (1.5 / 	(1/3));
		} else {
			this.shadowOffsetX = (this.radius * this.dxInt);
			this.shadowOffsetY = (this.radius * this.dyInt);
		}

	};

	this.left = function() {
		this.dxy= [1,0];
		this.styles();		
		this.y = [this.canvasHeight/2]; 
		this.x = [this.offsetXNeg];
	};	

	this.right = function() {
		this.dxy= [-1,0];
		this.styles();		
		this.y = [this.canvasHeight/2];
		this.x = this.offsetXPos;
	};	
	
	this.bottom = function() {
		this.dxy= [0,-1];
		this.styles();		
		this.y = [this.offsetYPos];
		this.x = this.canvasWidth / 2;
	};	

	this.top = function() {
		this.dxy= [0,1];
		this.styles();		
		this.y = [this.offsetYNeg];
		this.x = [this.canvasWidth / 2];
	};	
	
	this.topRight = function() {
		this.dxy= [-1,0.33];
		this.styles();		
		this.x = [this.offsetXPos];
		this.y = [this.offsetYNeg * 0.5];
	};

	this.topLeft = function() {
		this.dxy= [1,0.33];
		this.styles();		
		this.x = [this.offsetXNeg];
		this.y = [this.offsetYNeg * 0.5];
	};

	this.bottomRight = function() {
		this.dxy= [-1,-0.33];
		this.styles();		
		this.x = [this.offsetXPos];
		this.y = [this.offsetYPos * 0.5];
	};

	this.bottomLeft = function() {
		this.dxy= [1,-0.33];
		this.styles();		
		this.x = [this.offsetXNeg];
		this.y = [this.offsetYPos * 0.5];
	};
	Object.defineProperty(this, 'debug', {
		get: function() { 
			this.passes+=1;
			this.t2 = (performance.now() - this.t1) / 1000;
			this.tTotal = this.tTotal + this.t2;
			this.tAvg = this.tTotal / this.passes;
			
			if (this.t2 < this.tMin) {
				this.tMin = this.t2;
				this.$debugTable.find('#debugMin').html(this.tMin.toFixed(3) + ' sec');
			}
			if (this.t2 > this.tMax) {
				this.tMax = this.t2;
				this.$debugTable.find('#debugMax').html(this.tMax.toFixed(3) + ' sec');
			}

			switch(this.direction) {
				case 0: this.dirTxt = 'top left'; 	  this.topLeft(); 	  break;
				case 1: this.dirTxt = 'top'; 		  this.top(); 		  break;
				case 2: this.dirTxt = 'top right'; 	  this.topRight();    break;
				case 3: this.dirTxt = 'right'; 		  this.right(); 	  break;
				case 4: this.dirTxt = 'bottom right'; this.bottomRight(); break;
				case 5: this.dirTxt = 'bottom'; 	  this.bottom(); 	  break;
				case 6: this.dirTxt = 'bottom left';  this.bottomLeft();  break;
				case 7: this.dirTxt = 'left'; 		  this.left(); 		  break;
			}
			
			this.$debugTable.find('#debugX').html(this.x.toFixed(2));
			this.$debugTable.find('#debugY').html(this.y.toFixed(2));
			this.$debugTable.find('#debugDX').html(this.dx.toFixed(2));
			this.$debugTable.find('#debugDY').html(this.dy.toFixed(2));
			
			this.$debugTable.find('#debugPasses').html(this.passes);
			this.$debugTable.find('#debugTime').html(this.t2.toFixed(3) + ' sec');
			this.$debugTable.find('#debugAvg').html(this.tAvg.toFixed(3) + ' sec');

			
			this.$debugTable.find('#debugDir').html(this.dirTxt);
			console.log('Pass: ' + this.passes );
			console.log('Time: ' + this.t2.toFixed(3) + ' seconds...');
			console.log(' Avg: ' + this.tAvg.toFixed(3) + ' seconds...');
			console.log(' Min: ' + this.tMin.toFixed(3) + ' seconds...');
			console.log(' Max: ' + this.tMax.toFixed(3) + ' seconds...');
			console.log('----------------------------');
			console.log('Moving from ' + this.dirTxt);
		
			this.direction = (this.direction < 7) ? this.direction + 1 : 0;
			this.t1 = performance.now();
			return this.t2; 
		},
		set: function(t){ 
			if (t>0) {
				this.t1 = performance.now();
				console.log('Debugging...');
			}
			if (! this.passes ) {
				this.direction = 0;
				this.dirTxt = 'left';
				this.passes = 0;
				this.tTotal = 0;
				this.tAvg = 0;
				this.tMax = 0;
				this.tMin = 100000000000;
				this.$debugTable = $('<table></table>').attr('id', 'debugTable');
				
				$tr = $('<tr></tr>');
				$tdTitle = $('<td></td>').html('Title');
				$tdContent = $('<td></td>').attr('id', 'id').html('Title');

				$dir = $tr.clone();
				$dir.append($tdTitle.html('Dir:').clone());
				$dir.append($('<td></td>').attr('id', 'debugDir').html('left'));
				this.$debugTable.append($dir);
				
				$radius = $tr.clone();
				$radius.append($tdTitle.html('r:').clone());
				$radius.append($('<td></td>').attr('id', 'debugRadius').html(this.radius));
				this.$debugTable.append($radius);				
				
				$x = $tr.clone();
				$x.append($tdTitle.html('x:').clone());
				$x.append($('<td></td>').attr('id', 'debugX').html(this.x.toFixed(2)));
				this.$debugTable.append($x);

				$y = $tr.clone();
				$y.append($tdTitle.html('y:').clone());
				$y.append($('<td></td>').attr('id', 'debugY').html(this.y.toFixed(2)));
				this.$debugTable.append($y);

				$dx = $tr.clone();
				$dx.append($tdTitle.html('dx:').clone());
				$dx.append($('<td></td>').attr('id', 'debugDX').html(this.dx.toFixed(2)));
				this.$debugTable.append($dx);

				$dy = $tr.clone();
				$dy.append($tdTitle.html('dy:').clone());
				$dy.append($('<td></td>').attr('id', 'debugDY').html(this.dy.toFixed(2)));
				this.$debugTable.append($dy);
				
				$passes = $tr.clone();
				$passes.append($tdTitle.html('Pass:').clone());
				$passes.append($('<td></td>').attr('id', 'debugPasses').html(this.passes));
				this.$debugTable.append($passes);

				$time = $tr.clone();
				$time.append($tdTitle.html('Time:').clone());
				$time.append($('<td></td>').attr('id', 'debugTime').html('0.000 sec'));
				this.$debugTable.append($time);
				
				$avg = $tr.clone();
				$avg.append($tdTitle.html('Avg:').clone());
				$avg.append($('<td></td>').attr('id', 'debugAvg').html('0.000 sec'));
				this.$debugTable.append($avg);				

				$min = $tr.clone();
				$min.append($tdTitle.html('Min:').clone());
				$min.append($('<td></td>').attr('id', 'debugMin').html('0.000 sec'));
				this.$debugTable.append($min);				

				$max = $tr.clone();
				$max.append($tdTitle.html('Max:').clone());
				$max.append($('<td></td>').attr('id', 'debugMax').html('0.000 sec'));
				this.$debugTable.append($max);				


				this.$debugTable.appendTo('body');
			}
		}
	});
}

function animate() {
	window.requestAnimationFrame(flab.draw);
}

FlabsWorth.prototype = new Drawable();