/**
 * Abstract class for drawable objects.
 */
function Drawable() {
	this.scale = 1;
	this.speed = 1;
	this.blank = true;
	this.id    = null;
	this.name  = 'circle';
	this.dx    = 0;
	this.dy    = 0; 

	this.canvasWidth  = 0;
	this.canvasHeight = 0;

	this.textColor   = 'white';
	this.fillColor   = '#333';
	this.strokeColor = 'black';

	
	this.init  = function(x, y, width, height) {
		// Initialise canvas
		this.canvasWidth  = this.scaleVal(width);
		this.canvasHeight = this.scaleVal(height);		
		this.canvas = $('<canvas></canvas>')
			.attr('id', this.id)
			.prependTo($('body'));
		this.canvas[0].width  = this.canvasWidth;
		this.canvas[0].height = this.canvasHeight;

		// Get the drawing context
		this.ctx = this.canvas[0].getContext('2d');
		this.ctx.scale(1,1);

		// Call the subInit method
		this.subInit();

		// Get the styles for this actor
		this.styles();
	};
	
	this.subInit = function(){}; // Sub initialise method
	this.draw = function(){}; // Default drawing
	this.styles = function(){
		this.lineWidth     = 0;
		this.fillStyle     = 0;
		this.strokeStyle   = 0;
		this.shadowBlur    = 0;
		this.shadowColor   = 0;
	}; // Apply drawing styles

	this.scaleVal = function(val){ return val * this.scale; };
	this.circle = function() {
		this.clear();
		this.ctx.beginPath();
		this.ctx.arc(
			this.x, 	//x
			this.y,		//y
			this.radius,//r
			0,			//Start
			2*Math.PI);	//Finish
		// Draw and fill.
		this.ctx.stroke();
		this.ctx.fill();
		
		// Do text
		this.ctx.textAlign = 'center';
		textSize = this.radius / 4;
		this.ctx.font = textSize + "px Arial";
		this.fillStyle = this.textColor;
		this.shadowColor   = "rgba(55,55,55,0.0)";
		this.ctx.fillText(this.name, this.x, this.y + (textSize/2) + 2);

		// Revert to defaults...
		this.fillStyle = this.fillColor;
		this.shadowColor   = "rgba(55,55,55,0.25)";

		this.blank = false;
	};
	this.clear = function () {
		if (! this.blank ) {
			this.ctx.clearRect(0, 0, this.canvasWidth, this.canvasHeight);
			this.blank = true;
		}
	};
	Object.defineProperty(this, 'canvasWH', {
		set: function(cwh) {
			this.canvasWidth = this.scaleVal(cwh[0]);
			this.canvasHeight = this.scaleVal(cwh[1]);
		}
	});
	// x, y setters and getters
	Object.defineProperties(this, {
		x: {
			get: function() { return this.posX; },
			set: function(x) {
				if (x.constructor === Array) {
					this.posX   = x[0];
					this.startX = x[0];
				} else {
					this.posX = x; 
				}
			}
		},
		y: {
			get: function() { return this.posY; },
			set: function(y) {
				if (y.constructor === Array) {
					this.posY   = y[0];
					this.startY = y[0];
				} else {
					this.posY = y; 
				}
			}
		},
		dxy: {
			set: function(dxy) { 
				this.dx = this.scaleVal(dxy[0]); 
				this.dy = this.scaleVal(dxy[1]); 
			}
		},
		dxInt: {
			get: function(){
				if(0 > this.dx) {
					return -1;
				} else if (0 < this.dx) {
					return 1;
				} else {
					return 0;
				}
			}
		},
		dyInt: {
			get: function(){
				if(this.dy < 0) {
					return -1;
				} else if (this.dy > 0) {
					return 1;
				} else {
					return 0;
				}
			}
		},
	});

	Object.defineProperties(this, {
		inBounds: {
			get: function(){
				return (
					this.x >= this.offsetXNeg &&
					this.x <= this.offsetXPos &&
					this.y >= this.offsetYNeg &&
					this.y <= this.offsetYPos
				) ? true : false; } 
		},
		offsetX: {
			get: function(){ return this.radius + Math.abs(this.shadowOffsetX) + this.scaleVal(100);}
		},
		offsetY: {
			get: function(){ return this.radius + Math.abs(this.shadowOffsetY) + this.scaleVal(100);}
		},
		offsetXNeg: {
			get: function(){ return - this.offsetX;}
		},
		offsetYNeg: {
			get: function(){ return - this.offsetY;}
		},
		offsetXPos: {
			get: function(){ return this.canvasWidth + this.offsetX;}
		},
		offsetYPos: {
			get: function(){ return this.canvasHeight + this.offsetY;}
		},
		shadowOffsetX:  {
			get: function(){ return this.ctx.shadowOffsetX;},
			set: function(x){ this.ctx.shadowOffsetX = x;}
		},
		shadowOffsetY: {
			get: function() { return this.ctx.shadowOffsetY; },
			set: function(offsetY) { this.ctx.shadowOffsetY = offsetY; }
		},
		shadowBlur : {
			get: function() { return this.ctx.shadowBlur; },
			set: function(shadowBlur) { this.ctx.shadowBlur = shadowBlur; }
		},
		shadowColor : {
			get: function() { return this.ctx.shadowColor; },
			set: function(shadowColor) { this.ctx.shadowColor = shadowColor; }
		},
		lineWidth : {
			get: function() { return this.ctx.lineWidth; },
			set: function(lineWidth) { this.ctx.lineWidth = lineWidth; }
		},
		fillStyle : {
			get: function() { return this.ctx.fillStyle; },
			set: function(fillStyle) { this.ctx.fillStyle = fillStyle; }
		},
		strokeStyle : {
			get: function() { return this.ctx.strokeStyle; },
			set: function(strokeStyle) { this.ctx.strokeStyle = strokeStyle; }
		}
	});

}