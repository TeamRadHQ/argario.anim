var gulp     = require('gulp');
var stylus   = require('gulp-stylus');
var cleanCSS = require('gulp-clean-css');
var beautify = require('gulp-cssbeautify');
var prefixer = require('gulp-autoprefixer');
var nib      = require('nib');

gulp.task('styles', function() {
    gulp.src('css/app.styl')
        .pipe(stylus({ 
        	use: nib() 
        }))
        .pipe(cleanCSS({
        	compatibility: 'ie8', 
        	keepBreaks:    true,
        }))
		.pipe(prefixer({
			browsers: ['last 2 versions', 'ie 6-8'],
			cascade:  false
		}))
		.pipe(beautify())
        .pipe(gulp.dest('./css/'));
});

gulp.task('watch:styles', function() {
    gulp.watch('**/*.styl', ['styles']);
});
