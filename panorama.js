(function(){
	var speed = 10,
		initSpeed = speed;
	var doc = document,
		bgImg = new Image(),
		bgCanvas = doc.getElementById('bgCanvas'),
		bgCtx    = bgCanvas.getContext('2d'),
		fgCanvas = doc.getElementById('canvas'),
		fgCtx    = fgCanvas.getContext('2d'),
		p1Canvas = doc.getElementById('p1Canvas'),
		p1Ctx    = p1Canvas.getContext('2d'),
		death = doc.getElementById('death'),
		deathOpacity = 0,
		p1Size = p1Canvas.height/16;

		bgCtx.scale(1,1);
		fgCtx.scale(1,1);
		p1Ctx.scale(1,1);
	p1Canvas.isBlank = false;
	function player(x, y, size) {
		// p1Ctx.clearRect(0,0, fgCtx.width, fgCtx.height);
		p1Ctx.beginPath();
		p1Ctx.shadowBlur=10;
		p1Ctx.shadowOffsetY=10;
		// p1Ctx.shadowOffsetX=;
		p1Ctx.shadowColor="rgba(55,55,55,0.25)";
		p1Ctx.clearRect(0, 0, p1Canvas.width, p1Canvas.height);
		p1Ctx.arc(x, y, size, 0,Math.PI*2);
		p1Ctx.fillStyle = "#ffdddd";
		p1Ctx.strokeStyle = "#ff6666";
		p1Ctx.lineWidth = "3";
		p1Ctx.stroke();
		p1Ctx.fill();
	}
	var offset = fgCanvas.width + 1500;
	fgCanvas.isBlank = true;
	
	function eatMe(x) {
		if (x > -offset && x < fgCanvas.width * 1.75) {
			if (fgCanvas.isBlank) {
				fgCanvas.isBlank = false;
			}
			fgCtx.clearRect(0, 0, fgCanvas.width, fgCanvas.height);
			fgCtx.beginPath();
			fgCtx.shadowBlur=100;
			fgCtx.shadowOffsetY=10;
			fgCtx.shadowOffsetX=1500;
			fgCtx.shadowColor="rgba(55,55,55,0.25)";
			fgCtx.clearRect(0, 0, p1Canvas.width, p1Canvas.height);
			fgCtx.arc(x, fgCanvas.height / 2,fgCanvas.height, 0,Math.PI*2);
			fgCtx.fillStyle = "blue";
			fgCtx.strokeStyle = "navy";
			fgCtx.lineWidth = "10";
			fgCtx.stroke();
			fgCtx.fill();		
		} else if ( ! fgCanvas.isBlank ) {
			fgCtx.clearRect(0, 0, fgCanvas.width, fgCanvas.height);
			fgCanvas.isBlank = true;
			return;
		}
	}

	bgImg.src = 'img/background.gif';
	
	var	x  = 0,
		y  = 0,
		dy = 0.1 * speed, // Direction to increment
		bigCircle = - fgCanvas.width * 2.5;

	bgImg.onload = function() {
		var height = bgCanvas.height,
			width = bgCanvas.width;
		bgCanvas.cols = Math.ceil(bgCanvas.width / bgImg.width);
		bgCanvas.rows = Math.ceil(bgCanvas.height / bgImg.height);
		window.requestAnimationFrame(draw);
	};
	var t1 = performance.now();
	function draw(rows, cols) {
		// Draw the background
		bgCtx.clearRect(0,0, bgCanvas.width, bgCanvas.height);
		for (cc=0; cc < bgCanvas.cols; cc++) {
			for ( rr=0; rr < bgCanvas.rows; rr++) {
				// Draw image at col.x and row.y
				bgCtx.drawImage(
					bgImg,
					cc * bgImg.width,
					y +rr * bgImg.height,
					bgImg.width,
					bgImg.height
				);
				// Draw an image bgImg.height behind this
				bgCtx.drawImage(
					bgImg,
					cc * bgImg.width,
					y - (1 * bgImg.height),
					bgImg.width,
					bgImg.height
				);
				// Draw an image 2 * bgImg.height behind this
				bgCtx.drawImage(
					bgImg,
					cc * bgImg.width,
					y - (2 * bgImg.height),
					bgImg.width,
					bgImg.height
				);
			}
		} // End for col
		// Reset Scroll if exceeding bgCanvas height
		if ( y < bgCanvas.rows * bgImg.height - 1) {
			y+=dy;
		} else {
			y=0;
		}
		// Before the halfway point
		if (bigCircle < fgCanvas.width / 2) {
			// Send in the big cirlce
			bigCircle+=1 * speed;
			eatMe(bigCircle);
			player(p1Canvas.width / 2, p1Canvas.height / 2, p1Size);
			if (p1Size < p1Canvas.height/4) {
				p1Size += 0.05 * speed;
			}
		// After to the end
		} else if (bigCircle < fgCanvas.width * 2 ) {
			eatMe(bigCircle);
			bigCircle+=2 * speed;
			if ( ! p1Canvas.isBlank ) {
				p1Ctx.clearRect(0, 0, p1Canvas.width, p1Canvas.height);
				p1Canvas.isBlank = true;
			}
			death.style.opacity = deathOpacity;
			if (deathOpacity < 1) {
				deathOpacity += 0.0025 * speed;
			}
		} else if (speed > 0) {
			speed = speed - (initSpeed/100);
		//Stop animating
		} else {
			var t2 = (performance.now() - t1)/1000;
			console.log('Animation took ' + t2.toFixed(2) + ' seconds to complete.');
			return;
		}
		window.requestAnimationFrame(draw);
	}
}());