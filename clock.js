function clock() {
	var now = new Date(),
		ctx = document.getElementById('canvas').getContext('2d');
	ctx.save();
	ctx.clearRect(0,0,300,300);
	ctx.translate(150,150);
	ctx.scale(0.4,0.4);
	ctx.rotate(-Math.PI/2);
	ctx.strokeStyle = "black";
	ctx.fillStyle = "white";
	ctx.lineWidth = 8;
	ctx.lineCap = "round";

	// Hour Marks
	ctx.save();
	for (var ii = 0; ii < 12; ii++) {
		ctx.beginPath();
		ctx.rotate(Math.PI/6);
		ctx.moveTo(290,0);
		ctx.lineTo(320,0);
		ctx.stroke();
	}
	ctx.restore();
	
	// Minute Marks
	ctx.save();
	ctx.lineWidth = 2;
	for (ii=0; ii < 60; ii++) {
		if (ii%5!=0) {
			ctx.beginPath();
			ctx.moveTo(295,0);
			ctx.lineTo(315,0);
			ctx.stroke();
		}
		ctx.rotate(Math.PI/30);
	}
	ctx.restore();

	// Current Time
	var sec = now.getSeconds(),
		min = now.getMinutes(),
		hr  = now.getHours();
	// Convert 24 to 12 hour.
	hr = hr >= 12 ? hr-12 : hr;

	// Write hours
	ctx.save();
	ctx.rotate( hr * (Math.PI / 6) + (Math.PI/360) * min + (Math.PI/21600) * sec );
	ctx.lineWidth = 19;
	ctx.beginPath();
	ctx.moveTo(-5, 0);
	ctx.lineTo(150,0);
	ctx.stroke();
	ctx.restore();

	// Write Minutes
	ctx.save();
	ctx.rotate( (Math.PI/30) * min + (Math.PI/180) * sec );
	ctx.lineWidth = 14;
	ctx.beginPath();
	ctx.moveTo(-5,0);
	ctx.lineTo(225,0);
	ctx.stroke();
	ctx.restore();

	// Write Seconds
	ctx.save();
	ctx.rotate(sec * Math.PI/30);
	ctx.strokeStyle = "#D40000";
	ctx.fillStyle = "#D40000";
	ctx.lineWidth = 10;
	ctx.beginPath();
	ctx.moveTo(-5,0);
	ctx.lineTo(300,0);
	ctx.stroke();
	ctx.restore();

	ctx.beginPath();
	ctx.lineWidth = 14;
	ctx.strokeStyle = "#325FA2";
	ctx.arc(0,0,340,0,Math.PI*2,true);
	ctx.stroke();
	ctx.restore();

	window.requestAnimationFrame(clock);
}
window.requestAnimationFrame(clock);